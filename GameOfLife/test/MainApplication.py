# Main Application
    #Create New Window and Define yours Properties

from Tkinter import *
from MenuGameApplication import *

class MainApplication:

	def __init__( self ):

		self.MainWindow = Tk()      #Create New Window
	        self.MainWindow.title( "Game Of Life" )
		self.MainWindow.geometry( '300x100' )
		self.MainWindow.resizable( False, False )
        	self.MainWindow["bg"] = "GRAY"

		self.CanvasMainWindow = Canvas( self.MainWindow )
		self.CanvasMainWindow["bg"] = "GRAY"
		self.CanvasMainWindow["highlightbackground"] = "BLACK"
        	self.CanvasMainWindow.pack()          # Show Window in Pack Format

		self.ButtonPlay = Button( self.CanvasMainWindow )
		self.ButtonPlay["bg"] = "WHITE"
		self.ButtonPlay["highlightbackground"] = "WHITE"
		self.ButtonPlay["bd"] = 0
		self.ButtonPlay["width"] = 10
		self.ButtonPlay["text"] = "Play Game"
		self.ButtonPlay["command"] = self.PlayGame
		self.ButtonPlay.place( x = 30, y = 35 )
		self.ButtonPlay.focus_force()

		self.MainWindow.mainloop()

	def PlayGame( self ):

		self.CanvasMainWindow.pack_forget()
		MenuGame = MenuGameApplication( self.MainWindow )
		MenuGame.mainloop()



if __name__ == '__main__':

	MainApplication()
