# Standard Random
	# Create The Structure of Standard Random Game mode

from Tkinter import *
from random import *

GridSize = 15

class StandardRandom:

	def __init__( self, RandomWindow, Canvas ):

		self.RandCanvas = Canvas
		self.RandWindow = RandomWindow

		self.RandCanvas.delete('all')

		self.Matrix = []

		leftTopBotPoints = 0
		rightTopBotPoints = 15

		topStaticPoint = 0
		botStaticPoint = 15

		for line in range( 0, 43 ):

			for collun in range( 0, 80 ):

				self.Grid = [leftTopBotPoints,topStaticPoint,leftTopBotPoints,botStaticPoint,rightTopBotPoints,botStaticPoint,rightTopBotPoints,topStaticPoint]

				if( randint( 0, 9 ) == 2 or randint( 0, 9 ) == 7):

					matrixTemp = self.RandCanvas.create_polygon(self.Grid , fill='BLACK', outline='WHITE', tag='BlackBox')

					self.Matrix.append( 1 )

				else:

					matrixTemp = self.RandCanvas.create_polygon(self.Grid , fill='', outline='WHITE', tag='WhiteBox')

					self.Matrix.append( 0 )

				leftTopBotPoints += 15
				rightTopBotPoints += 15

			topStaticPoint += 15
			botStaticPoint += 15

			leftTopBotPoints = 0
			rightTopBotPoints = 15

	def RunGameOfLife( self ):

		while( True ):

			if( self.Matrix[10] == 1):
				print("A")
				matrixTemp = self.RandCanvas.create_polygon(self.Grid , fill='', outline='WHITE', tag='WhiteBox')
				self.Matrix[0] = 0
			else:
				print("B")
				matrixTemp = self.RandCanvas.create_polygon(self.Grid , fill='BLACK', outline='WHITE', tag='BlackBox')
				self.Matrix[0] = 0

			self.RandWindow.after( 70 )
			self.RandWindow.update()
