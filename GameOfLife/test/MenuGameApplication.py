# Menu Game Application
    #Create Menu Options and Define this Modes

from Tkinter import *
from StandardBasicAplication import *

class MenuGameApplication:

	def __init__( self, MenuGameWindow ):

		MenuGameWindow.title( "Game Of Life >> Menu Game" )
		MenuGameWindow.geometry( '350x145' )

		self.MGWindow = MenuGameWindow

		self.CanvasMenuGame = Canvas( MenuGameWindow )
		self.CanvasMenuGame["bg"] = "GRAY"
		self.CanvasMenuGame["highlightbackground"] = "BLACK"
        	self.CanvasMenuGame.pack()          # Show Window in Pack Format

		self.ButtonStdRandom = Button( self.CanvasMenuGame )
		self.ButtonStdRandom["bg"] = "WHITE"
		self.ButtonStdRandom["highlightbackground"] = "BLACK"
		self.ButtonStdRandom["bd"] = 0
		self.ButtonStdRandom["width"] = 20
		self.ButtonStdRandom["text"] = "Standard Random"
		self.ButtonStdRandom["command"] = self.ActionStandardRandom
		self.ButtonStdRandom.place( x = 30, y = 30 )

		self.ButtonStdReady = Button( self.CanvasMenuGame )
		self.ButtonStdReady["bg"] = "WHITE"
		self.ButtonStdReady["highlightbackground"] = "BLACK"
		self.ButtonStdReady["bd"] = 0
		self.ButtonStdReady["width"] = 20
		self.ButtonStdReady["text"] = "Standard Ready"
		self.ButtonStdReady["command"] = self.ActionStandardReady
		self.ButtonStdReady.place( x = 30, y = 60 )

		self.ButtonStsDraw = Button( self.CanvasMenuGame )
		self.ButtonStsDraw["bg"] = "WHITE"
		self.ButtonStsDraw["highlightbackground"] = "BLACK"
		self.ButtonStsDraw["bd"] = 0
		self.ButtonStsDraw["width"] = 20
		self.ButtonStsDraw["text"] = "Standard Draw"
		self.ButtonStsDraw["command"] = self.ActionStandardDraw
		self.ButtonStsDraw.place( x = 30, y = 90 )

	def ActionStandardRandom( self ):

		self.CanvasMenuGame.pack_forget()
		StandardBasic = StandardBasicApplication( self.MGWindow, self.CanvasMenuGame, 0 )
		StandardBasic.mainloop()

	def ActionStandardReady( self ):

		self.CanvasMenuGame.pack_forget()
		StandardBasic = StandardBasicApplication( self.MGWindow, self.CanvasMenuGame, 1 )
		StandardBasic.mainloop()

	def ActionStandardDraw( self ):

		self.CanvasMenuGame.pack_forget()
		StandardBasic = StandardBasicApplication( self.MGWindow, self.CanvasMenuGame, 2 )
		StandardBasic.mainloop()
